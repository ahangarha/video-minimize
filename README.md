# About Video-minimize

This is a personal script meant to ease size reduction of video tutorials and
make them more portable to carry on mobile phone.

This Script gets a list of video files and decreases their scale and fps, then
removes the original ones.

# Installation

There is no need to install this script. Simply copy the script in `bin`
directory in HOME directory (in you don't have `bin` directory there, make it)

# Usage

There are two ways to use this script:

1. Direct:

    `$ video-minimize file1 file2 file3 ...`

2. Indirect (pipe)

    `$ ls *.mp4 | video-minimize`

    or

    `$ find . -iname "*.mp4" -exec video-minimize {} +`

# TODO
 - [ ] add option support for:
    - [ ] `-k --keep`: keep the original files
    - [ ] `-h --help`: Show quick help
 - [ ] Show progress indicator for each individual process
 - [ ] Show n/m, indicating overal progress of processing multiple files

# License

This script is under GPLv3 license. Feel free to use, change and contrubute :)
